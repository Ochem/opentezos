var fs = require('fs');
const path = require('path');
var variables = require('../variables.json');

function injectVariables(file) {
  fs.readFile(file, 'utf-8', function(err, data){
    if (err) throw err;

    var newValue = data.replace(/\[:?([A-Z_@]+):\]/g, (_, innerGroup) => {
        const splitted = innerGroup.split('@');
        var variable = variables[splitted[0]];
        switch (splitted[1]) {
          case "UPP":
            variable = variable.toUpperCase();
            break;
          case "LOW":
            variable = variable.toLowerCase();
            break;
          case "CAP":
            variable = variable.charAt(0).toUpperCase() + variable.slice(1)
            break
          default:
            break;
        }
        return variable || splitted[0];
      }
    );
    
    fs.writeFile(file, newValue, 'utf-8', function (err) {
      if (err) throw err;
    });
  });
}

function throughDirectory(directory, transformer) {
    fs.readdirSync(directory).forEach((file) => {
        const absolutePath = path.join(directory, file);
        if (fs.statSync(absolutePath).isDirectory()) return throughDirectory(absolutePath, transformer);
        else if (path.extname(absolutePath) == '.md') transformer(absolutePath);
    });
}

/**
 * @param {string} src  The path to the thing to copy.
 * @param {string} dest The path to the new copy.
 */
var copyRecursiveSync = function(src, dest) {
    var exists = fs.existsSync(src);
    var stats = exists && fs.statSync(src);
    var isDirectory = exists && stats.isDirectory();
    if (isDirectory) {
        fs.mkdirSync(dest);
        fs.readdirSync(src).forEach(function(childItemName) {
        copyRecursiveSync(path.join(src, childItemName),
                            path.join(dest, childItemName));
        });
    } else {
        fs.copyFileSync(src, dest);
    }
};

const path_prebuild = path.resolve(__dirname, '../prebuild/');

// Clean previous prebuild
fs.rmSync(path_prebuild, { recursive: true, force: true });

// Copy all files under ../docs/ to ../prebuild/
copyRecursiveSync(path.resolve(__dirname, '../docs/'), path_prebuild);

// Apply the variable injection in every .md files
throughDirectory(path_prebuild, injectVariables);
