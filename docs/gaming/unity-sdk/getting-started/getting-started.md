---
id: getting-started
title: Getting started
authors: Matthew Roberts, Efe Kucuk, Felipe Cardozo
---

# Getting Started with the Tezos SDK for Unity

Start by checking out [Getting started video](https://youtu.be/0ouzNVxYI9g).

In this guide we will cover:

- Installing of the Unity Editor
- Adding Tezos SDK for Unity into a Unity project
- Getting a Tezos compatible wallet 
- Creating a wallet account for a test network (`ghostnet`)
- Linking this wallet account with the new Unity project

When you finish this guide, your game will be fully connected to the Tezos blockchain with your wallet address. After this point, you can start experimenting with various functionalities.

If you want to start exploring in a sample project, check out our [Inventory Sample Game](/gaming/unity-sdk/inventory-sample-game).

## Installing the Unity Editor 

If this is your first time using Unity, you'll need to [download the Unity Hub and Unity Editor](https://learn.unity.com/tutorial/install-the-unity-hub-and-editor). 


>Tezos SDK for Unity is compatible with Unity versions **2021.3 and 2022.3**.

Tezos SDK for Unity supports games on: 
- Web
- PC
- iOS
- Android
- Mac

If you are in fact brand-new to Unity, we highly recommend following the tutorials provided by Unity. You can find the Unity tutorials [here.](https://learn.unity.com/) 


### Create a New Unity Project

  Once you're ready to go, open Unity Hub and click `Create a new project:`

<p align="center">

![NewProject](./NewProject.png)

<small className="figure">Unity Hub: New Project</small>

</p>

Next, select 3D or 2D, and give your project a name, and click `Create Project`.



### Installing the Tezos SDK for Unity

#### Install from a Git URL
You can install the SDK directly via Git URL. To load a package from a Git URL:

* Open [Unity Package Manager](https://docs.unity3d.com/Manual/upm-ui.html) window (Window Menu bar > Package Manger).
* Click the add **+** button in the status bar.
* The options for adding packages appear.
* Select `Add package from git URL` from the add menu. Then a text box and an Add button appear.
* Enter the address `https://github.com/trilitech/tezos-unity-sdk.git` on `Git URL` in the text box and click Add.

>Alternatively, you can visit our [Unity Asset Store](https://assetstore.unity.com/packages/decentralization/infrastructure/tezos-sdk-for-unity-239001) page to install the SDK.


## Configuring the project settings for each platform

A few extra steps are needed so that you can successfully build your projects for **Mobile and Web platforms**.

#### Mobile settings (iOS and Android)

If you plan the use the SDK on mobile platforms (IOS or Android) you need to build your project with at least Minimal Managed Stripping Level, otherwise you may encounter [assembly errors](https://github.com/trilitech/tezos-unity-sdk/issues/90).

You can do this with:

- Open Edit Menu bar > Project Settings.
- Navigate to Player tab.
- Chose platform-specific settings (Android or IOS).
- Make sure that `Managed Stripping Level` is set as `disabled` or `minimal`. (It's in optimization section.)

#### WebGL settings

For the WebGL platform, Unity uses an index.html template that is to be hosted. When you open the project for the first time, it selects a default index.html template.

Instead, you should change the settings so that it uses a template we created for the project.

To do this:
- go to Player Settings -> Resolution and Presentation
- set  WebGL Template slot to “Airgap”

<p align="center">

![WebGL Project settings](./webgl-project-settings.png)

<small className="figure">Project Settings: WebGL Template slot to “Airgap”</small>

</p>


## Getting a Tezos-Compatible Wallet on Your Local Device

#### First: Why Do We Need A Wallet?

In the world of Web3, a 'wallet' is essential for storing and managing cryptocurrencies like Bitcoin, ETH, or Tezos' own currency, XTZ or Tez. However, it serves a broader purpose in blockchain technology. For our Tezos-integrated Unity games, as well as other blockchain-enabled applications like distributed finance or NFT minting websites, the wallet grants users permission to execute blockchain transactions on their behalf.

In the context of your game or dApp, the wallet's public key, also known as the wallet address, acts as a unique identifier, akin to a "userid." By scanning a QR code or linking the game to the wallet, users demonstrate ownership of this wallet address. This pairing is completed when the user actively accepts the connection on their wallet. Subsequent in-game transactions, such as buying or selling in-game NFTs, will also require explicit confirmation on the paired wallet.

As a game developer, you'll need a Tezos-compatible wallet for development and testing purposes. Popular Tezos-compatible software wallets include Kukai, Temple, Umami, and Airgap. For this guide, we'll use the [Temple](https://templewallet.com/) wallet, which has both mobile app and browser extension versions.

#### Tezos-Compatible Wallets

Our scenario will assume a 'hot' or internet-connected software wallet residing on some local device.  Major Tezos-compatible software wallets at the moment include Kukai, Temple, Umami and Airgap.  Not all of these wallets have both mobile app and browser-extension versions, and having both of these may be handy, depending on your target game platform.  The **Temple** wallet has both a mobile app and browser extensions, and we will use it in the present guide.  But any Tezos-compatible wallet will work if it can be paired with the game in at least one way.

### Getting started with Temple Wallet

#### 1. Download
Visit the [Temple](https://templewallet.com/) website, and download the mobile app of your choice, Android or iOS. You can also download the extensions on web for testing purposes.

#### 2. Account Creation
When you launch the Temple wallet app, you'll be prompted to create a new wallet account or import an existing one. If you don't have a wallet account for **Ghostnet Tez (the test network)**, choose to create a new account.

#### 3. Select Tezos
During the account creation process, you'll be asked for the blockchain type. Choose Tezos as the blockchain for your new account.

#### 4. Secure Your Wallet
The creation process generates a unique 12 words **seed phrase** for recovery purposes. Store this phrase securely in case you need to recover your wallet. Additionally, create a strong password (8+ characters) to unlock your wallet, as you'll use it frequently.

#### 5. Wallet Address: 
The **public key hash** will serve as your wallet address in our documentation. This is the identifier you'll use for your game or dApp to interact with the wallet.

 Here is an Android phone with the Temple app open, showing the two different accounts currently on the wallet:

<p align="center">

![two wallet accounts](./two_wallet_accounts.png)
<small className="figure">Two wallet accounts</small>

</p>

Once you've successfully created one account in your Temple wallet, you'll likely want to create at least one more so you can test sending or buying in-game tokens back and forth between them.  

As you see, in the case of multiple accounts on a single wallet, each of these accounts has its *own* wallet address, shown above in blue.  Tezos wallet accounts always begin with "tz-": the full hash strings are typically never shown in the wallet app, but instead the leading and trailing characters are displayed with elipses between them.  This display is always a link and so can be copied and pasted elsewhere as needed.  Here is the full string for the second account shown above: 

<p align="center">

![wallet address](./wallet_address.png)
<small className="figure">A example of a wallet address.</small>

</p>

### Getting XTZ / Tez on Testnet

Once you've created one or more accounts, you'll want to obtain some free **Test Tezos** for the Tezos Ghostnet test chain, at a minimum to pay for the gas & storage fees incurred as you test transactions while developing your own Tezos-integrated game.  You can do this by navigating in a browser to this address: https://faucet.ghostnet.teztnets.xyz/, where you'll be presented with an interface like the following:

<p align="center">

![ghostnet funding ui](./ghostnet_funding_ui.png)
<small className="figure">Faucet: Ghostnet faucet without a wallet connected</small>

</p>



As you see, you have several options here.  The easiest would be to **Connect Wallet**, which will invite you to pair with an existing wallet account, such as the one you've just created.  You'll get this dialogue box:

<p align="center">

![pair wallet dialog -- ghostnet](./pair_wallet_dialog_--_ghostnet.png)
<small className="figure" >Beacon: Pair wallet dialog</small>

</p>

If your newly created Temple wallet is installed as a **browser plugin**, you would want to choose it as the second option in the list above.  If that wallet is instead installed on a mobile device, you'll want to use the **Pair wallet on another device** button at the bottom, which will present you with a QR code for you to scan from your mobile Temple wallet app.  Either way, you'll need to confirm the pairing from within the chosen wallet, at which point the main Ghostnet interface will change to one like the following:

<p align="center">

![ghostnet faucet w wallet connected](./ghostnet_faucet_w_wallet_connected.png)
<small className="figure" >Faucet: Ghostnet faucet with a wallet connected</small>

</p>

Now your full wallet account string is displayed under "My wallet", and again below "Ghostnet faucet".  In general Web3 parlance, a 'faucet' is a web source from which you can obtain free cryptocurrency.  In the earlier days of crypto, one could obtain small amounts of *real* cryptocurrency (i.e. actual worth money) from some such faucets, but those days are gone for the more established crypto coins.  However Tezos, like other chains, maintains a strong interest in encouraging app development on their chain without the developer having to spend real money to pay for development transaction fees; hence this Ghostnet faucet source for test Tez, which will only work with contracts published to the Tezos Ghostnet chain.  You could request a single Tez coin here, but you might as well get the full 6001 test-Tez (certainly if you want to test spending or sending Tez balances).  

Note that there is also an option here to send test Tez from this web page to any arbitrary Tezos wallet address.  This option would be useful if you wanted to send Ghostnet test Tez to a teammate's wallet account, or if you wanted to find another test account of our own that wasn't currently paired.

However you choose to receive your test Tez, be aware that this Tez exists only on the Ghostnet chain.  Here there is an important difference between the current Temple browser plugin and mobile app installs.  A Temple *browser* wallet that's just been paired with the faucet above will show this:

<p align="center">

![temple browser plugin default view](./temple_browser_plugin_default_view.png)
<small className="figure" >Temple: Wallet on browser extension</small>

</p>

Note that at the top it displays Account 2 (we have two accounts in this wallet as well) because that's the one that was used for the pairing, and then in the drop-down immediately below it displays "Ghostnet Testnet" because we just finished using the faucet above and received 6001 test Tez, and this transaction was conducted on the Ghostnet network.  This balance is also reflected in the Tezos box.  If we were to click on this network drop-down, it would display a long list of potential networks, both Tezos and for other chains that the Temple wallet supports.  If in that drop-down, we were to select Tezos **Mainnet**, our displayed balance would fall to 0 because we have not bought real Tez (with real money) for this account.  Again, Ghostnet test Tez exists *only* on that test network.

The handy thing about this Temple browser plugin is that we didn't even need to choose Ghostnet in the network drop-down before pairing this wallet with the Ghostnet faucet; in fact, that wallet had never paired with any app on Ghostnet before.  Simply by specifying this wallet account address for pairing, the browser wallet knew to flip itself to the correct network.  Incidentally, the same happens if we use this browser-plugin wallet to pair with our game app; it will automatically choose the correct network for the pairing.  As of this writing, though, Temple has not added this network drop-down yet to their *mobile* Temple app.   So it will instead be necessary to explicitly add the **Ghostnet RPC node** to this app.  Let's go back to the Temple mobile app, now with the full controls revealed in the bottom icon:


<p align="center">

![temple mainnet](./temple_mainnet.png)
<small className="figure">Temple Mobile: Mainnet Wallet without faucet</small>

</p>

Let's click on the "Settings" control icon at the bottom icon, to reveal the settings options:

<p align="center">

![temple settings](./temple_settings.png)
<small className="figure">Temple mobile: Settings page</small>

</p>


Let's click on the **Default node (RPC)** setting, which is highlighted in the blue box in the screenshot. 

<p align="center">

![temple rpc list](./temple_rpc_list.png)
<small className="figure">Temple Mobile: RPC list page without Ghostnet network</small>

</p>

In the screenshot, you'll notice the topmost option labeled "Temple Default," which, in the case of this account, corresponds to the Tezos Mainnet. However, our current objective is not to connect to the Mainnet, except for instances when we need to access a game deployed on it. Instead, we aim to connect to Ghostnet. As you can see, Ghostnet is the option currently selected in the app, but this is because it has already been added to this account. Initially, when this account was created, Ghostnet wasn't even available in the list of options. To add Ghostnet, we had to add a custom RPC control, which will open the following dialogue box:

<p align="center">

![temple add rpc](./temple_add_rpc.png)
<small className="figure">Temple Mobile: Add RPC page</small>

</p>

`Name` is your own custom name/alias for this network, we happened to use 'Marigold Ghostnet' for Ghostnet. What you do need to get exactly right is the URL, since ultimately a blockchain RPC call is an http call to the server access node for that blockchain. 
`URL` is url of the node, we're using the Marigold Ghostnet node here, that is `https://ghostnet.tezos.marigold.dev/`. Once you do get it right and **Add** this RPC, it will show up in the list of custom RPC nodes again, with the new Ghostnet node, like:  

<p align="center">

![temple rpc list with ghostnet](./temple_rpc_list_w_ghostnet.png)
<small className="figure">Temple Mobile: RPC list page with Ghostnet network</small>

</p>

Although the process may appear cumbersome for the mobile side, and we hope that the features will become more streamlined and closely aligned with the browser plugin in the near future, there's a significant advantage once you've successfully added the Ghostnet RPC node to the list. The mobile wallet app will automatically switch to this node whenever it's paired with a dApp published on Ghostnet. Whether it's the Ghostnet faucet website or your own game app, the mobile wallet app will seamlessly connect to Ghostnet, making the overall experience smoother and more convenient.

<p align="center">

![temple ghostnet](./temple_ghostnet.png)
<small className="figure" >Temple Mobile: Ghostnet wallet page with faucet</small>

</p>

## Link your Wallet with the New Unity Project

Nearly all of the pieces are now in place for your Unity project to begin interacting with the Tezos blockchain.  If you now return to your Unity project where we've imported the Tezos SDK, a good way to start is importing our sample project. On Package Manager windows, you need to select our SDK and import this sample, as follows:

<p align="center">

![unity import sample](./unity_import_sample.png)
<small className="figure" >Temple Mobile: Ghostnet wallet page with faucet</small>

</p>

Let's start using the premade scene `NftApiSample`, to do this, open the scene folder in the Tezos Unity SDK
sample, as follows:

<p align="center">

![unity project](./unity_project.png)
<small className="figure" >Temple Mobile: Ghostnet wallet page with faucet</small>

</p>

After locating the scene, we need to move it to the Unity Hierachy and delete the other schemes, as follows:

<p align="center">

![unity hierarchy](./unity_hierarchy.png)
<small className="figure" >Temple Mobile: Ghostnet wallet page with faucet</small>

</p>

Now, we need to run the scene, to do this, we need to click on the play button, and you will see the following QRCode on screen:

<p align="center">

![unity start](./unity_start.png)
<small className="figure" >Temple Mobile: Ghostnet wallet page with faucet</small>

</p>

With your mobile device, you need to scan the QRCode, and connect with Temple or any compatible wallet. Just press
**confirm** button on the following screen:

<p align="center">

![temple login](./temple_login.png)
<small className="figure" >Temple Mobile: Ghostnet wallet page with faucet</small>

</p>

After this, on your game you will see the following screen meaning that you are authenticated successfully:

<p align="center">

![unity authenticated](./unity_authenticated.png)
<small className="figure" >Temple Mobile: Ghostnet wallet page with faucet</small>

</p>

At this point, you'll have successfully linked your Unity app with a crypto wallet, which means that you can now use this pairing to make transaction and other calls between the Unity app and the Tezos network.  The remainder of this document set is intended to help you do this.

Start experimenting, and happy developing!

 If you are interested in exploring further, check out our [Inventory Sample Game](/gaming/unity-sdk/inventory-sample-game)!



